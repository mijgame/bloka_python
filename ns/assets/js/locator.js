class Locator {
    constructor() {
        this.geocoder = new google.maps.Geocoder();
    }

    find(hints) {
        this.search = '';

        if (hints.address) {
            this.search += hints.address + ' ';
        }

        if (hints.city) {
            this.search += hints.city + ' ';
        }

        if (hints.country) {
            this.search += hints.country + ' '
        }

        return new Promise((res, rej) => this.__invoke(res, rej));
    }

    __invoke(resolve, reject) {
        /*
            We can't directly reference the __process function as the arguments since
            the 'this'-context will be wrong in the called function.
         */
        this.geocoder.geocode({address: this.search}, (results, status) => {
            let location = this.__process(results, status);

            if (location !== [] && ! location) {
                reject(location);
            }

            resolve(location);
        });
    }

    __findType(component) {
        const checks = {
            street_number: 'street_number',
            route: 'route',
            city: 'locality',
            province: 'administrative_area_level_1',
            country: 'country',
            zipcode: 'postal_code'
        };

        let result = {};
        _.forOwn(checks, (v, k) => {
            if (_.includes(component.types, v)) {
                result[k] = component.long_name;
            }
        });

        return result;
    }

    __process(results, status) {
        if (status !== 'OK') {
            return results;
        }

        let result = results[0];

        let location = _.assign.apply(_,
            result.address_components.map(this.__findType)
        );

        location.lat = result.geometry.location.lat();
        location.long = result.geometry.location.lng();

        return location;
    }
}

module.exports = new Locator;