class Router {
    constructor() {
        this.service = new google.maps.DirectionsService()
    }

    route(source, destination, display) {
        this.source = source;
        this.destination = destination;
        this.display = display;

        return new Promise((res, rej) => this.__execute(res, rej));
    }

    __execute(resolve, reject) {
        let req = {
            origin: this.source,
            destination: this.destination,
            travelMode: 'DRIVING'
        };

        this.service.route(req, (result, status) => {
            if (status !== 'OK') {
                return reject(result);
            }

            resolve(result);
        });
    }
}

module.exports = new Router();