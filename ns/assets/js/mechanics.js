class Mechanics {
    constructor() {
        this.resolved = false;
        this.mechanics = [];
        this.fetchPromise = this.__fetch();
    }

    get() {
        return new Promise((resolve, reject) => {
            if (this.resolved) {
                return resolve(this.mechanics);
            }

            this.fetchPromise
                .then(() => resolve(this.mechanics))
                .catch(reject);
        });
    }

    create(attributes) {
        let query = `
            mutation(
              $firstname: String, $lastname: String, $zipcode: String, $street: String
                $city: String, $province: String, $country: String, $phone: String
              $long: Float, $lat: Float
            ) {
              createMechanic(
                firstname: $firstname
                lastname: $lastname
                zipcode: $zipcode
                street: $street
                city: $city
                province: $province
                country: $country
                phone: $phone
                long: $long
                lat: $lat
              ) {
                mechanic {
                  firstname
                  lastname
                  city
                }
                ok
              }
            }`;

        return Client.query(query, attributes);
    }

    update(attributes) {
        let query = `
            mutation(
              $id: ID, $firstname: String, $lastname: String, $zipcode: String, $street: String
              $city: String, $province: String, $country: String, $phone: String
              $long: Float, $lat: Float
            ) {
              updateMechanic(
                id: $id
                firstname: $firstname
                lastname: $lastname
                zipcode: $zipcode
                street: $street
                city: $city
                province: $province
                country: $country
                phone: $phone
                long: $long
                lat: $lat
              ) {
                ok
              }
            }`;

        return Client.query(query, attributes);
    }

    destroy(id) {
        const query = `
            mutation($id: ID) {
              destroyMechanic(id: $id) {
                ok
              }
            }`;

        return Client.query(query, {id});
    }

    refresh() {
        this.__fetch().then(() => {
            Bus.$emit('mechanics.refreshed', this.mechanics);
        });
    }

    __fetch() {
        this.resolved = false;

        const query = `
            query {
                mechanics {
                    id
                    firstname
                    lastname
                    city
                    street
                    phone
                    zipcode
                    province
                }
            }`;

        return Client.query(query)
            .then(r => this.mechanics = r.mechanics)
            .then(() => this.resolved = true);
    }
}

let instance = new Mechanics();

// Automatically update the list of mechanics every 5 seconds
setInterval(() => instance.refresh(), 5000);

module.exports = instance;