const Lokka = require('lokka').Lokka;
const Transport = require('lokka-transport-http').Transport;

let csrfToken = Cookies.get('csrftoken');
let httpTransport = new Transport('/graphql');

/*
Documentation says this should be in the constructor for the transport,
but that doesn't work. This will work.
 */
httpTransport._httpOptions.headers['X-CSRFToken']  = csrfToken;

module.exports =  new Lokka({
    transport: httpTransport
});

