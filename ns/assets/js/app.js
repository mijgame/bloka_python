require('lodash');
require('jquery');
require('semantic-ui-css');
require('semantic-ui-calendar/dist/calendar');
require('./scheduler');

window.moment = require('moment');
window.moment.locale('nl');

window.Vue = require('vue');
window.axios = require('axios');
window.Cookies = require('js-cookie');

// Set CSRF token as a default axios header.
window.axios.defaults.headers.common['X-CSRFToken'] = Cookies.get('csrftoken');

window.Client = require('./graphql');

window.Map = require('./map');
window.Mechanics = require('./mechanics');
window.Stations = require('./stations');
window.Router = require('./router');
window.Malfunctions = require('./malfunctions');

window.Bus = new Vue();
window.App = new Vue({
    el: '#app',
    components: {
        'p-map': require('./components/Map.vue'),
        'p-mechanics': require('./components/Mechanics.vue'),
        'p-routes': require('./components/Routes.vue'),
        'p-reports': require('./components/Reports.vue'),
        'p-schedule': require('./components/Schedule.vue'),
    }
});