class Stations {
    constructor() {
        this.resolved = false;
        this.stations = [];
        this.fetchPromise = this.__fetch();
    }

    get() {
        return new Promise((resolve, reject) => {
            if (this.resolved) {
                return resolve(this.stations);
            }

            this.fetchPromise
                .then(() => resolve(this.stations))
                .catch(reject);
        })
    }

    __fetch() {
        this.resolved = false;

        const query = `
            query {
              stations {
                lat
                lon
                country
                names {
                  short
                  long
                }
              }
            }`;

        return Client.query(query)
            .then(this.__filter)
            .then(s => this.stations = s)
            .then(() => this.resolved = true);
    }

    __filter({stations}) {
        // We only want Dutch stations
        return _.filter(stations, s => s.country === 'NL');
    }
}

module.exports = new Stations();