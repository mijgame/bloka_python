require('dhtmlx-scheduler');

scheduler.locale = {
    date: {
        month_full: ["Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"],
        month_short: ["Jan", "Feb", "Maa", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"],
        day_full: ["Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag"],
        day_short: ["Zon", "Maa", "Din", "Woe", "Don", "Vrij", "Zat"]
    },
    labels: {
        dhx_cal_today_button: "Vandaag",
        day_tab: "Dag",
        week_tab: "Week",
        month_tab: "Maand",
        new_event: "Nieuwe planning",
        icon_save: "Opslaan",
        icon_cancel: "Annuleren",
        icon_details: "Details",
        icon_edit: "Bewerken",
        icon_delete: "Verwijderen",
        confirm_closing: "", //Your changes will be lost, are your sure?
        confirm_deleting: "Planning wordt permanent verwijderd, verwijderen?",
        section_description: "Beschrijving",
        section_time: "Tijdsperiode"
    }
};