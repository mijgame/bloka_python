class Map {
    constructor() {
        this.map = null;
        this.options = {
            zoom: 8,
            center: {lat: 52.132633, lng: 5.2912659999999505} // Lat en lng are for The Netherlands
        };
    }

    get() {
        if (! this.map) {
            return console.error('Map needs to be created before using it!');
        }

        return this.map;
    }

    create(element, options = {}) {
        if (! element) {
            return console.error('The element given for map creation is not set!');
        }

        if (! this.map) {
            this.map = new google.maps.Map(element, _.assign(this.options, options));
        }

        // Fix gray map bug
        setTimeout(() => {
            google.maps.event.trigger(this.map, 'resize');
        }, 1000);
    }

    resetView() {
        this.map.setZoom(this.options.zoom);
        this.map.setCenter(this.options.center);
    }
}

module.exports = new Map();