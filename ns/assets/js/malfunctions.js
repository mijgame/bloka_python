class Malfunctions {
    constructor() {
        this.resolved = false;
        this.malfunctions = [];
        this.fetchPromise = this.__fetch();
    }

    get() {
        return new Promise((resolve, reject) => {
            if (this.resolved) {
                return resolve(this.malfunctions);
            }

            this.fetchPromise
                .then(() => resolve(this.malfunctions))
                .catch(reject);
        });
    }

    create(attributes) {
        let query = `
            mutation($station: String, $reportedAt: DateTime) {
                createMalfunction(
                    station: $station
                    reportedAt: $reportedAt
                ) {
                    ok
                }
            }`;

        return Client.query(query, attributes);
    }

    destroy(id) {
        const query = `
            mutation($id: ID) {
                destroyMalfunction(id: $id) {
                    ok
                }
            }
        `;

        return Client.query(query, {id});
    }

    refresh() {
        this.__fetch().then(() => {
            Bus.$emit('malfunctions.refreshed', this.malfunctions);
        });
    }

    __fetch() {
        this.resolved = false;

        const query = `
            query {
                malfunctions {
                    id
                    station
                    reportedAt
                }
            }`;

        return Client.query(query)
            .then(r => this.malfunctions = r.malfunctions)
            .then(() => this.resolved = true);
    }
}

let instance = new Malfunctions();

// Automatically update the list of mechanics every 5 seconds
setInterval(() => instance.refresh(), 5000);

module.exports = instance;