import telegram


class Bot:
    """
    The Bot class provides a simple wrapper around the Telegram bot used for sending messages to Mechanics.
    It exposes one function: send_message, that simply requires the text (HTML) to be send to Telegram.
    """

    def __init__(self):
        """
        Set up the Telegram bot.
        """
        self.bot = telegram.Bot(token='424245720:AAFsu2OiH2eZ6p_2Ovfi3anZMjT4AluzW2Q')
        self.chat_id = -260418660

    def send_message(self, message):
        """
        Send a message to the Telegram group chat for mechanics.

        >>> Bot().send_message(None)
        Traceback (most recent call last):
        ...
        TypeError: Expected str, got NoneType

        >>> Bot().send_message(25)
        Traceback (most recent call last):
        ...
        TypeError: Expected str, got int

        >>> Bot().send_message("test")
        ...

        >>> Bot().send_message("<b>test</b>")
        ...

        >>> Bot().send_message("<p>test</p>")
        Traceback (most recent call last):
        ...
        telegram.error.BadRequest: Can't parse entities in message text: unsupported start tag "p" at byte offset 0

        :param message:
        :return:
        """
        if type(message) is not str:
            raise TypeError('Expected str, got ' + type(message).__name__)

        self.bot.send_message(chat_id=self.chat_id,text=message,parse_mode=telegram.ParseMode.HTML)
