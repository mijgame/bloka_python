import googlemaps


class Directions:
    """
    The Directions class provides a simple wrapper around the Google Maps Distance Matrix api.
    It offer the 'find' method for quickly getting a result from the Distance Matrix API.
    The Google Maps API key is handled internally in the class.
    """

    def __init__(self):
        """
        Create the Google Maps client with the API token.
        """
        self.client = googlemaps.Client(key='AIzaSyAwenrkkL9lEi3d_dlIYa1kCMmAQ9KGJMs')

    def find(self, source, destination):
        """
        Given a source and a destination, find the distance between these points.
        The source and destination can be any valid source and destination per Google documentation, such
        as a address string or lat/lng combination.

        Source and address strings could be specified as follows:
        Hoendercamp 2, Zevenaar, The Netherlands
        Daltonlaan 200, Utrecht

        The country is not strictly necessary but can prevent ambiguity.
        The return value is formatted as follows (values are examples):
        {
            'distance': {
                'text': '78.6 km',
                'value': 78637
            },
            'duration' {
                'text': '54 mins',
                'value': 3266
            },
            'status': 'OK'
        }

        >>> Directions().find('Hoendercamp 2, Zevenaar, The Netherlands', 'Daltonlaan 200, Utrecht, The Netherlands') # doctest: +ELLIPSIS
        {'distance': {'text': '78,6 km', 'value': 78638}, 'duration': {'text': '55 min.', 'value': 3270}, 'status': 'OK'}

        >>> Directions().find(None, None)
        Traceback (most recent call last):
        ...
        TypeError: Expected a lat/lng dict or tuple, but got NoneType

        >>> Directions().find((51.922740, 6.061069), 'Daltonlaan 200, Utrecht, The Netherlands')
        {'distance': {'text': '78,6 km', 'value': 78638}, 'duration': {'text': '55 min.', 'value': 3270}, 'status': 'OK'}

        >>> Directions().find((51.922740, 6.061069), (52.087169, 5.159741))
        {'distance': {'text': '78,7 km', 'value': 78689}, 'duration': {'text': '55 min.', 'value': 3286}, 'status': 'OK'}

        :param source:
        :param destination:
        :return:
        """
        result = self.client.distance_matrix([source], [destination], mode="driving", language="nl-NL")

        # We only care about the first result
        return result['rows'][0]['elements'][0]
