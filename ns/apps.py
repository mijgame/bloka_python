from django.apps import AppConfig


class NsConfig(AppConfig):
    """
    Configuration for the NS Django application.
    """
    name = 'ns'
