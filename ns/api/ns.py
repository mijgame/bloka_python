from requests import get
import xmltodict


class NS:
    """
        This is a wrapper class that provides a central location
        for the API credentials to live.
    """

    AUTH = None

    @staticmethod
    def initialize(username, password):
        """
        Set the username and password required for retrieving information
        from the NS API.

        :param username:
        :param password:
        :return:
        """
        NS.AUTH = (username, password)

    def get(self, url):
        """
        Execute a request to the given URL with the credentials
        initialized in the 'initialize' function.

        :param url:
        :return:
        """
        if NS.AUTH is None:
            raise Exception('API username and password not set.')

        result = get(url, auth=NS.AUTH).content
        return xmltodict.parse(result, dict_constructor=dict)
