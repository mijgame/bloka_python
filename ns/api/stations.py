from .ns import NS


class Stations(NS):
    """
    Class for all API calls related to NS stations.
    """
    def get_all(self):
        """
        Get a list of all stations.

        :return:
        """
        return self.get('https://webservices.ns.nl/ns-api-stations-v2')
