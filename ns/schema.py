import graphene

from ns.schemas.malfunctions import MalfunctionType, CreateMalfunction, DestroyMalfunction
from .models import Mechanic, ScheduleItem, Malfunction
from .api.stations import Stations

from .schemas.stations import StationType
from .schemas.mechanics import MechanicType, CreateMechanic, DestroyMechanic, UpdateMechanic


class QueryType(graphene.ObjectType):
    """
    The QueryType is type that graphene (GraphQL) will use when it gets a query (instead of a mutation).
    It contains all possible queries for the GraphQL-endpoint.
    """
    name = 'Query'
    description = '...'

    mechanics = graphene.List(MechanicType)
    mechanic = graphene.Field(
        MechanicType,
        id=graphene.ID()
    )

    malfunctions = graphene.List(MalfunctionType)
    malfunction = graphene.Field(
        MalfunctionType,
        id=graphene.ID()
    )

    stations = graphene.List(StationType)

    @graphene.resolve_only_args
    def resolve_mechanics(self):
        """
        This will simply return all mechanics from the database.
        In the future, pagination may added.

        :return:
        """
        return Mechanic.objects.all()

    @graphene.resolve_only_args
    def resolve_malfunctions(self):
        """
        This will all malfunctions that are stored in the database.

        :return:
        """
        return Malfunction.objects.all()

    @graphene.resolve_only_args
    def resolve_stations(self):
        """
        Here, the Stations api from NS is queried for all stations.
        No filtering is applied here, the only thing that happens is that it simplifies
        what is returned by accessing the Station-node before returning.

        :return:
        """
        return Stations().get_all()['Stations']['Station']

    @graphene.resolve_only_args
    def resolve_schedule(self):
        """
        Load the entire schedule.
        Will be changed or removed in the future.

        :return:
        """
        return ScheduleItem.objects.all()

    def resolve_schedule_item(self, args, context, info):
        """
        Resolve a specific schedule item by its id.

        :param args:
        :param context:
        :param info:
        :return:
        """
        id = args.get('id')
        return ScheduleItem.objects.get(pk=id)

    def resolve_malfunction(self, args, context, info):
        """
        Resolve a specific malfunction by its id.

        :param args:
        :param context:
        :param info:
        :return:
        """
        id = args.get('id')
        return Mechanic.objects.get(pk=id)

    def resolve_mechanic(self, args, context, info):
        """
        Resolve a mechanic by his or her id.

        :param args:
        :param context:
        :param info:
        :return:
        """
        id = args.get('id')
        return Mechanic.objects.get(pk=id)


class MutationType(graphene.ObjectType):
    """
    The MutationType is a collection of all mutation-related GraphQl queries
    for this GraphQL-endpoint.
    """
    create_mechanic = CreateMechanic.Field()
    update_mechanic = UpdateMechanic.Field()
    destroy_mechanic = DestroyMechanic.Field()

    create_malfunction = CreateMalfunction.Field()
    destroy_malfunction = DestroyMalfunction.Field()


"""
This represents the final schema that will be generated for
this GraphQL endpoint.
"""
schema = graphene.Schema(
    query=QueryType,
    mutation=MutationType
)
