import graphene


class StationNameType(graphene.ObjectType):
    """
    Type that allows for subquerying of station names.
    """
    short = graphene.String()
    middle = graphene.String()
    long = graphene.String()

    def resolve_short(self, args, context, info):
        return self['Kort']

    def resolve_middle(self, args, context, info):
        return self['Middel']

    def resolve_long(self, args, context, info):
        return self['Lang']


class StationType(graphene.ObjectType):
    """
    Class that is used to represent a train station as returned
    by the NS Stations API.
    """
    name = 'Station'
    description = 'A train station.'

    code = graphene.String()
    type = graphene.String()

    country = graphene.String()
    uiccode = graphene.String()

    lat = graphene.Float()
    lon = graphene.Float()

    names = graphene.Field(StationNameType)
    synonyms = graphene.List(graphene.String)

    '''
        All data for the stations originates from the NS api.
        This means everything, including Tag names, is in Dutch.
        In order to keep everything english and camelcase, each field is resolved
        individually.
    '''
    def resolve_code(self, args, context, info):
        return self['Code']

    def resolve_type(self, args, context, info):
        return self['Type']

    def resolve_lat(self, args, context, info):
        return float(self['Lat'])

    def resolve_lon(self, args, context, info):
        return float(self['Lon'])

    def resolve_names(self, args, context, info):
        return self['Namen']

    def resolve_country(self, args, context, info):
        return self['Land']

    def resolve_uiccode(self, args, context, info):
        return self['UICCode']

    def resolve_synonyms(self, args, context, info):
        # A train station doesn't necessarily have a synonym
        if self['Synoniemen'] is None:
            return None

        # When a train station has only one one synonym, it will be a string.
        # This needs to be converted to list, otherwise graphene will convert to string to a list of letters
        if isinstance(self['Synoniemen']['Synoniem'], str):
            return [self['Synoniemen']['Synoniem']]

        # Otherwise, the train station has multiple synonyms which are handled correctly
        return self['Synoniemen']['Synoniem']
