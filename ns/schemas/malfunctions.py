import graphene
from graphene.types.datetime import DateTime

from ns.models import Malfunction


class MalfunctionType(graphene.ObjectType):
    """
    The underlying malfunction type defined for all queries and mutations.
    It represents the Malfunction model from the database.
    """

    name = 'Malfunction'
    description = 'A malfunction in a ticket machine.'

    id = graphene.ID()
    station = graphene.String()
    reported_at = DateTime()


class CreateMalfunction(graphene.Mutation):
    """
      Represents the mutation query for the creation of a new malfunction in the database.
      """
    class Input:
        station = graphene.String()
        reported_at = DateTime()


    ok = graphene.Boolean()
    malfunction = graphene.Field(lambda: MalfunctionType)

    def mutate(self, info, input, context):
        """
        Create a new malfunction in the database with the
        parameters given.

        :param info:
        :param input:
        :param context:
        :return:
        """
        malfunction = Malfunction.objects.create(**info)
        ok = True
        return CreateMalfunction(malfunction=malfunction, ok=ok)


class DestroyMalfunction(graphene.Mutation):
    """
    Represents the mutation query for deleting a existing malfunction from the database.
    """
    class Input:
        id = graphene.ID()

    ok = graphene.Boolean()

    def mutate(self, info, input, context):
        Malfunction.objects.filter(id=info.get('id')).delete()
        return DestroyMalfunction(ok=True)
