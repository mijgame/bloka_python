import graphene
from ..models import Mechanic


class MechanicType(graphene.ObjectType):
    """
    The underlying mechanic type defined for all queries and mutations.
    It represents the Mechanic model from the database.
    """
    name = 'Mechanic'
    description = 'A mechanic repairs broken equipment.'

    id = graphene.ID()
    firstname = graphene.String()
    lastname = graphene.String()
    zipcode = graphene.String()
    street = graphene.String()
    city = graphene.String()
    province = graphene.String()
    country = graphene.String()
    phone = graphene.String()
    long = graphene.Float()
    lat = graphene.Float()


class CreateMechanic(graphene.Mutation):
    """
    Represents the mutation query for the creation of a new mechanic in the database.
    """
    class Input:
        firstname = graphene.String()
        lastname = graphene.String()
        zipcode = graphene.String()
        street = graphene.String()
        city = graphene.String()
        province = graphene.String()
        country = graphene.String()
        phone = graphene.String()
        long = graphene.Float()
        lat = graphene.Float()

    ok = graphene.Boolean()
    mechanic = graphene.Field(lambda: MechanicType)

    def mutate(self, info, input, context):
        """
        Create a new mechanic in the database with the
        parameters given.

        :param info:
        :param input:
        :param context:
        :return:
        """
        mechanic = Mechanic.objects.create(**info)
        ok = True
        return CreateMechanic(mechanic=mechanic, ok=ok)


class UpdateMechanic(graphene.Mutation):
    """
    Represents the mutation query for updating a existing mechanic in the database.
    The difference in input-parameters with the CreateMechanic type is that a id is required.
    """
    class Input:
        id = graphene.ID()
        firstname = graphene.String()
        lastname = graphene.String()
        zipcode = graphene.String()
        street = graphene.String()
        city = graphene.String()
        province = graphene.String()
        country = graphene.String()
        phone = graphene.String()
        long = graphene.Float()
        lat = graphene.Float()

    ok = graphene.Boolean()

    def mutate(self, info, input, context):
        # We don't want to update the primary key in the database
        id = info.pop('id', None)

        Mechanic.objects.filter(id=id).update(**info)

        ok = True
        return UpdateMechanic(ok=ok)


class DestroyMechanic(graphene.Mutation):
    """
    Represents the mutation query for deleting a existing mechanic from the database.
    """
    class Input:
        id = graphene.ID()

    ok = graphene.Boolean()

    def mutate(self, info, input, context):
        Mechanic.objects.filter(id=info.get('id')).delete()
        return DestroyMechanic(ok=True)
