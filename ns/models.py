from django.db import models

# Prevent the 'key too long' error on MySQL
max_str_length = 191


# Create your models here.
class Mechanic(models.Model):
    """
    The Mechanic is basically a combination of a person model and an location.
    There is no need to split this up into multiple tables, as this information will always be accessed
    together.
    """

    firstname = models.CharField(max_length=max_str_length)
    lastname = models.CharField(max_length=max_str_length)
    zipcode = models.CharField(max_length=max_str_length)
    street = models.CharField(max_length=max_str_length)
    city = models.CharField(max_length=max_str_length)
    province = models.CharField(max_length=max_str_length)
    country = models.CharField(max_length=max_str_length)
    phone = models.CharField(max_length=max_str_length)
    long = models.DecimalField(max_digits=9, decimal_places=6)  # precision up to 16cm
    lat = models.DecimalField(max_digits=9, decimal_places=6)  # precision up to 16cm

    def __str__(self):
        return self.firstname + ' ' + self.lastname


class Malfunction(models.Model):
    """
    The malfunction is a reported fault of a ticket machine at a given station.
    """

    # Short name of the station
    station = models.CharField(max_length=max_str_length)
    reported_at = models.DateTimeField()


class ScheduleItem(models.Model):
    """
    A schedule item is a single instance of a 'agenda item', with a start and end time (including day).
    It can span multiple days.

    Every schedule item has to linked to a specific mechanic. Schedule items are used to determine which mechanic
    the message should be send to when a card machine breaks.
    """
    start = models.DateTimeField()
    end = models.DateTimeField()
    mechanic = models.ForeignKey(Mechanic)
