from django.conf.urls import url
from graphene_django.views import GraphQLView

from . import views

"""
Here, all the URl patterns for the NS application are defined.
Most of the requests will route through the 'graphql' endpoint, as that is the
preferred way to query or mutate data. 

Other routes are used when GraphQL is not appropriate for the required behaviour.
"""
urlpatterns = [
    url(r'^$', views.index),
    url(r'^graphql', GraphQLView.as_view(graphiql=True)),
    url(r'^api/schedule/(?P<mechanic>[0-9]+)$', views.schedule_get),
    url(r'^api/schedule/(?P<mechanic>[0-9]+)/save$', views.schedule_save),
    url(r'^api/malfunction/report$', views.malfunction_report),
    url(r'^api/malfunction/finalize', views.malfunction_finalize)
]