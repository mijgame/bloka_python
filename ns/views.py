import datetime
import textwrap

import re

from .api.ns import NS
from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
from django.db import connection
from .models import ScheduleItem, Mechanic
from .bot import Bot
from .directions import Directions
from django.views.decorators.csrf import ensure_csrf_cookie

import simplejson

# Set up the NS API with the correct credentials
NS.initialize('lex.ruesink@student.hu.nl', 'ojxzKZi0FfZJHp1EBtlBqd2Y1YltCkGvHah2BtU0OS_TCh92js_66w')

# Initialize the Telegram bot
telegram_bot = Bot()


@ensure_csrf_cookie
def index(request):
    """
    Show the main page.

    :param request:
    :return:
    """
    return render(request, 'index.html')


def converter(o):
    """
    This is a converter function for simplejson that allows it to json-encode DateTime
    objects.

    :param o:
    :return:
    """
    if isinstance(o, datetime.datetime):
        return o.__str__()


def schedule_get(request, mechanic):
    """
    Retrieve the schedule for a given mechanic.

    :param request:
    :param mechanic:
    :return:
    """
    schedules = ScheduleItem.objects.raw('SELECT id, start, end FROM ns_scheduleitem WHERE mechanic_id = %s',
                                         [mechanic])
    print(schedules)
    deserialized = serializers.serialize('python', schedules, ensure_ascii=False)

    return HttpResponse(simplejson.dumps(deserialized, default=converter), content_type='application/json')


def schedule_save(request, mechanic):
    """
    Save the schedule for the given mechanic.
    This functions works using a changeset that it retrieves from the client.
    The changeset consists of three lists:
    - added
    - changed
    - removed

    The changeset is constructed on the client. This function interprets and executes
    this changeset.

    :param request:
    :param mechanic:
    :return:
    """

    # simplejson.loads only accepts a unicode string and the request body
    # is a binary string. So we decode it to unicode.
    body_unicode = request.body.decode('utf-8')
    body = simplejson.loads(body_unicode)

    schedules = body['schedules']

    print(schedules)

    model = Mechanic.objects.get(id=mechanic)

    # Create all new schedule items
    for added in schedules['added']:
        if added is None:
            continue

        item = ScheduleItem()
        item.start = added['start_date']
        item.end = added['end_date']
        item.mechanic = model
        item.save()

    # Change existing schedule items
    for changed in schedules['changed']:
        if changed is None:
            continue

        item = ScheduleItem.objects.get(id=int(changed['id']))
        item.start = changed['start_date']
        item.end = changed['end_date']
        item.save()

    # Remove all the extraneous schedule items
    for removed in schedules['removed']:
        if removed is None:
            continue

        ScheduleItem.objects.filter(id=int(removed)).delete()

    return HttpResponse('OK')


def malfunction_report(request):
    """
    When a malfunction is reported, this endpoint is called
    to determine the closest mechanics that are currently planned and
    send the message to the Telegram group.

    :param request:
    :return:
    """

    # GET.get allows a default empty string instead of a panic
    # if the parameter is not passed
    station = request.GET.get('station', '')
    lat = request.GET.get('lat', '')
    lon = request.GET.get('lon', '')

    # Check if the required arguments are present
    if station == '':
        return HttpResponse("Station is required", status=422)

    if lat == '':
        return HttpResponse("Lat is required", status=422)

    if lon == '':
        return HttpResponse("Lon is required", status=422)

    # A raw SQL query works better for this task, we fetch all mechanics that are currently
    # on call.
    cursor = connection.cursor()
    cursor.execute(
        'SELECT m.firstname, m.lastname, m.lat, m.long FROM ns_scheduleitem s, ns_mechanic m WHERE s.mechanic_id = m.id AND NOW() BETWEEN s.start AND s.end'
    )
    result = cursor.fetchall()

    # If there are no mechanics available we should make note of the
    # malfunction and return
    if not len(result):
        message = '''
                Er is een storing gemeld op station <b>{}</b>. Echter zijn er nu geen monteurs ingepland.
                Op het beheerspaneel kan de planning gewijzigd worden en dit bericht opnieuw worden uitgezonden.
                '''.format(station)

        telegram_bot.send_message(
            textwrap.dedent(message)
        )

        return HttpResponse('OK')

    finder = Directions()
    destination = (float(lat), float(lon))
    order = []

    # Determine the distance to the destination train station from the home location
    # of the mechanic.
    for mech in result:
        source = (float(mech[2]), float(mech[3]))
        distance = finder.find(source, destination)

        order.append({
            'name': '{} {}'.format(mech[0], mech[1]),
            'distance': distance['distance'],
            'duration': distance['duration']
        })

    # Sort by shortest distance ascending (shortest distance first)
    order.sort(key=lambda x: x['duration']['value'])

    # Create a formatted list of all mechanics
    list = ''
    for mech in order:
        list += '    - {}: {} reistijd ({})\n'.format(mech['name'], mech['duration']['text'], mech['distance']['text'])

    # Compose the final Telegram message
    message = '''
            Er is een storing gemeld op station <b>{}</b>.
            De volgende monteurs zijn ingepland, <b>{}</b> is de eerste keuze:
            {}
            Dit bericht kan vanuit het beheerspaneel opnieuw uitgezonden worden.
            '''.format(station, order[0]['name'], list)

    telegram_bot.send_message(
        re.sub('[ \t]+', ' ', message)
    )

    return HttpResponse("OK")


def malfunction_finalize(request):
    """
    Send a message to the Telegram group app that the malfunction
    at the given station is resolved.

    :param request:
    :return:
    """
    station = request.GET.get('station', '')

    if station == '':
        return HttpResponse("Station is required", status=422)

    message = 'De storing op station <b>{}</b> is opgelost.'.format(station)
    telegram_bot.send_message(message)

    return HttpResponse("OK")
